import axios from 'axios'
// 导入store里的index.js 因为要用这里的state中的token
import store from '../src/store/index.js'
const request = axios.create({
  baseURL: 'http://www.liulongbin.top:3008'
})
// 添加请求拦截器
request.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    // config就是请求的配置对象
    // 判断一下需要添加请求头的url地址
    if (config.url.startsWith('/my/')) {
      config.headers.Authorization = store.state.user.token
    }
    return config
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)
export default request
