import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/reg',
    component: () => import('@/views/Reg.vue')
  },
  {
    path: '/',
    // 懒加载方式
    component: () => import('@/views/Main.vue')
  }
]

const router = new VueRouter({
  routes
})
router.beforeEach((to, from, next) => {
  // to 是要访问的的地址
  // from 是要从什么地址离开
  // next 就是允许访问 是一个调用的函数next（）
  if (to.path === '/') {
    const token = localStorage.getItem('vuex')

    if (token) {
      next()
    } else {
      next('/login')
    }
  } else {
    next()
  }
})

export default router
