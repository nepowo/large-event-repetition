// 导入获取用户信息的方法
import { getUserAPI } from '../../api/reg.js'
export default {
  namespaced: true,
  state() {
    return {
      token: '',
      user: {}
    }
  },
  mutations: {
    // 修改tokne 的值 只能通过下面的方法
    updateToken(state, token) {
      state.token = token
    },
    // 更新user数据、
    updateUser(state, user) {
      state.user = user
    }
  },

  getters: {},
  actions: {
    async getUser(ctx) {
      const { data: res } = await getUserAPI()
      // console.log(res)
      if (res.code === 0) {
        ctx.commit('updateUser', res.data)
      }
    }
  }
}
