import request from '../utils/request.js'

export const regAPI = data => {
  return request.post('/api/reg', data)
}

export const loginApi = data => {
  return request.post('/api/login', data)
}
// 导出获取用户信息的方法
export const getUserAPI = () => request.get('/my/userinfo')
