module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ['plugin:vue/essential', '@vue/standard'],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    // 添加空格规则
    'space-before-function-paren': [
      'error',
      {
        anonymous: 'always',
        named: 'never', //带名字的函数允许不带空格
        asyncArrow: 'always'
      }
    ]
  }
}
